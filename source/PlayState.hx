package;

import flixel.FlxBasic;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.math.FlxPoint;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;
import flixel.util.FlxTimer.FlxTimerManager;
import rl.rlGen.RLGenerateMap;
import rl.RLDefs;
import rl.rlObjects.RLPlayer;
import rl.rlGen.RLTileTypes;
import haxe.EnumTools.EnumValueTools;

class PlayState extends FlxState
{
	private var RLcamera:FlxCamera;
	private var cameraFocus:FlxSprite;
	private var player:RLPlayer;
	private var mapArray: Array<Array<Int>>;
	private var _map: FlxTilemap;
	
	override public function create():Void
	{
		super.create();
		
		_map = new FlxTilemap();
		
		cameraFocus = new FlxSprite();
		cameraFocus.makeGraphic(1, 1, FlxColor.TRANSPARENT);
		add(cameraFocus);
		
		
		var levelType: Int = FlxG.random.weightedPick([20, 20, 20, 20, 10, 10]);
		trace("Level Type: " + levelType);
		Sys.sleep(2);
		var wide: Int = RLDefs.MAP_WIDTH;
		var high: Int = RLDefs.MAP_HEIGHT;
		switch (levelType)
		{
			case 0: _map = RLGenerateMap.generateCavern(wide, high);
			case 1: _map = RLGenerateMap.generateDungeon(wide, high, 15, 60, 10, 33, 10, 40, 15, 30, 2, 6, 50);
			case 2: _map = RLGenerateMap.generateDungeon(wide, high, 40, 60, 0, 15, 20, 20, 20, 30, 2, 4, 50);
			case 3: _map = RLGenerateMap.generateDungeon(wide, high, 33, 33, 12, 12, 33, 33, 25, 25, 3, 3, 25);
			case 4: _map = RLGenerateMap.generateDungeon(wide, high, 35, 35, 0, 5, 40, 60, 10, 25, 7, 9, 20);
			case 5: _map = RLGenerateMap.generateDungeon(wide, high, 10, 10, 0, 0, 50, 75, 0, 15, 3, 3, 30);
		}
		_map.setTileProperties(EnumValueTools.getIndex(TILE_ROOM), FlxObject.NONE);
		_map.setTileProperties(EnumValueTools.getIndex(TILE_O_DOOR), FlxObject.NONE);
		_map.follow(RLcamera);
		add(_map);
		
		var spawnX: Int = 0;
		var spawnY: Int = 0;
		
		var foundStart:Bool = false;
		while (!foundStart)
		{
			var spawnXtest, spawnYtest: Int;
			spawnX = FlxG.random.int(0, wide - 1);
			spawnY = FlxG.random.int(0, high - 1);
			if (_map.getTile(spawnX, spawnY) == 0)
			{
				foundStart = true;
			}
		}
		player = new RLPlayer(RLDefs.TILE_WIDTH * spawnX, RLDefs.TILE_HEIGHT * spawnY);
		player.loadGraphic("assets/images/player2.png", false, 24, 24);
		add(player);

		#if debug
		FlxG.watch.add(player, "x");
		FlxG.watch.add(player, "y");
		#end
		
		RLcamera = FlxG.camera;
		RLcamera.follow(player, FlxCameraFollowStyle.TOPDOWN, 10);
	}

	override public function update(elapsed:Float):Void
	{
		var prevX: Float;
		var prevY: Float;
		

		prevX = player.x;
		prevY = player.y;
		
		super.update(elapsed);
		
		if (cameraFocus.x < FlxG.width / 2)
		{
			cameraFocus.x = FlxG.width / 2;
		}
		if (cameraFocus.x > RLDefs.MAP_WIDTH * RLDefs.TILE_WIDTH - FlxG.width / 2)
		{
			cameraFocus.x = RLDefs.MAP_WIDTH * RLDefs.TILE_WIDTH - FlxG.width / 2;
		}
		if (cameraFocus.y < FlxG.height / 2)
		{
			cameraFocus.y = FlxG.height / 2;
		}
		if (cameraFocus.y > RLDefs.MAP_HEIGHT * RLDefs.TILE_HEIGHT - FlxG.height / 2)
		{
			cameraFocus.y = RLDefs.MAP_HEIGHT * RLDefs.TILE_HEIGHT - FlxG.height / 2;
		}
		if (player.overlaps(_map, false, RLcamera))
		{
			player.x = prevX;
			player.y = prevY;
		}
	}
}
