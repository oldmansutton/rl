package;

import flixel.FlxGame;
import flixel.FlxG;
import openfl.display.Sprite;

class Main extends Sprite
{
	public function new()
	{
		super();

		#if debug
		FlxG.log.redirectTraces = true;
		#end

		addChild(new FlxGame(1080, 720, MenuState, 1, 60, 60, true, false));
	}
}