package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import flixel.math.FlxMath;

class MenuState extends FlxState
{
	override public function create():Void
	{
		super.create();
		
		var init_x:Int = Math.floor(FlxG.width / 2 - 120);
		
		var btn_new = new FlxButton(init_x, 92, "Virtual Videogame Museum", onNew);
		var btn_load = new FlxButton(init_x, 120, "Quit", onLoad);
		btn_new.makeGraphic(240, 20, FlxColor.MAGENTA);
		btn_load.makeGraphic(240, 20, FlxColor.PURPLE);
		add(btn_new);
		add(btn_load);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
	
	private function onNew():Void 
	{
		FlxG.switchState(new PlayState());
	}
	
	private function onLoad():Void 
	{
		Sys.exit(0);
	}
	

}
