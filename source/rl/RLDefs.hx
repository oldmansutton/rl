package rl;

/**
 * ...
 * @author 
 */
class RLDefs
{
	static public var TILE_WIDTH:Int = 24;
	static public var TILE_HEIGHT:Int = 24;
	static public var MAP_WIDTH:Int = 50;
	static public var MAP_HEIGHT:Int = 30;
	static public var MOVE_DISTANCE:Int = 24;
	static public var ROOM_MIN:Int = 3;
	static public var ROOM_MAX:Int = 8;
}