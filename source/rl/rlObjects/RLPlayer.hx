package rl.rlObjects;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.util.FlxTimer;
import rl.RLDefs;

/**
 * ...
 * @author 
 */
class RLPlayer extends FlxSprite
{
	private var canMove: Bool;
	public function new(?X:Float=0, ?Y:Float=0, ?SimpleGraphic:FlxGraphicAsset) 
	{
		#if debug
		FlxG.watch.add(this, "x");
		FlxG.watch.add(this, "y");
		#end
		super(X, Y, SimpleGraphic);
		canMove = true;
	}
	
	public function resumeMove(?Timer:FlxTimer):Void
	{
		if (canMove == false) canMove = true;
	}
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		if (canMove) 
		{
			if (FlxG.keys.anyPressed(["NUMPADONE","NUMPADFOUR","NUMPADSEVEN"]))
			{
				x -= RLDefs.MOVE_DISTANCE;
				#if debug 
				trace("x: " + x);
				#end
				canMove = false;
			}
			if (FlxG.keys.anyPressed(["NUMPADTHREE","NUMPADSIX","NUMPADNINE"]))
			{
				x += RLDefs.MOVE_DISTANCE;
				#if debug 
				trace("x: " + x);
				#end
				canMove = false;
			}
			if (FlxG.keys.anyPressed(["NUMPADSEVEN","NUMPADEIGHT","NUMPADNINE"]))
			{
				y -= RLDefs.MOVE_DISTANCE;
				#if debug
				trace("y: " + y);
				#end
				canMove = false;
			}
			if (FlxG.keys.anyPressed(["NUMPADONE","NUMPADTWO","NUMPADTHREE"]))
			{
				y += RLDefs.MOVE_DISTANCE;
				#if debug
				trace("y: " + y);
				#end
				canMove = false;
			}
			if (FlxG.keys.anyJustReleased(["NUMPADONE", "NUMPADTWO", "NUMPADTHREE", "NUMPADFOUR", "NUMPADSIX", "NUMPADSEVEN", "NUMPADEIGHT", "NUMPADNINE"]))
			{
				canMove = true;
			}
			
			if (canMove == false)
			{
				new FlxTimer().start(.2, resumeMove, 1);
			}
		}
		if (FlxG.keys.anyJustPressed(["ESCAPE"]))
		{
			FlxG.switchState(new MenuState());
		}
	}
	
}