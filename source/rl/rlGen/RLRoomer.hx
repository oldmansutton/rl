package rl.rlGen;

import flixel.FlxG;
import flixel.math.FlxPoint;
import rl.RLDefs;

/**
 * ...
 * @author 
 */
class RLRoomer
{
	public var DirX: Int = 0;
	public var DirY: Int = 0;
	public var DoorPosX: Int = 0;
	public var DoorPosY: Int = 0;
	public var DimensionX: Int = 0;
	public var DimensionY: Int = 0;
	public var UpperLeft: FlxPoint;
	public var UpperRight: FlxPoint;
	public var LowerLeft: FlxPoint;
	public var LowerRight: FlxPoint;
	
	public function new(x: Int, y: Int, xd: Int, yd: Int) 
	{
		DirX = xd;
		DirY = yd;
		DoorPosX = x;
		DoorPosY = y;
		DimensionX = FlxG.random.int(RLDefs.ROOM_MIN, RLDefs.ROOM_MAX);
		DimensionY = FlxG.random.int(RLDefs.ROOM_MIN, RLDefs.ROOM_MAX);
	}
	
	public function getRoomCorners(offsetX:Int, offsetY:Int)
	{
		var top: Int;
		var bottom: Int;
		var left: Int;
		var right: Int;
		
		if (DirY == 0) 
		{
			if (DirX < 0)
			{
				right = DoorPosX - 1;
				left = DoorPosX - DimensionX - 1;
			} else {
				right = DoorPosX + DimensionX + 1;
				left = DoorPosX + 1;
			} 
			top = DoorPosY - DimensionY + offsetY;
			bottom = DoorPosY + offsetY;
		} else {
			if (DirY < 0)
			{
				top = DoorPosY - DimensionY - 1;
				bottom = DoorPosY - 1;
			} else {
				top = DoorPosY + 1;
				bottom = DoorPosY + DimensionY + 1;
			}
			right = DoorPosX + offsetX;
			left = DoorPosX - DimensionX + offsetX;
		}
		LowerLeft = new FlxPoint(left, bottom);
		LowerRight = new FlxPoint(right, bottom);
		UpperLeft = new FlxPoint(left, top);
		UpperRight = new FlxPoint(right, top);
	}
	
}