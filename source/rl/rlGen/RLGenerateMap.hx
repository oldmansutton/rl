package rl.rlGen;

import flash.display.InteractiveObject;
import flixel.FlxG;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.math.FlxRandom;
import flixel.tile.FlxTile;
import flixel.tile.FlxTilemap;
import flixel.tile.FlxPathfinding;
import flixel.FlxObject;
import rl.rlGen.RLRoomer;
import rl.rlGen.RLTunneler;
import rl.rlGen.RLTileTypes;
import rl.RLDefs;
import haxe.EnumTools;

class RLGenerateMap
{

	private static function fastFloor(v:Float) : Int 
	{
		return Std.int(v); // actually it's more "truncate" than "round to 0"
	}
	
	private static function getLine(x0:Int, y0:Int, x1:Int, y1:Int) : Array<{x:Int, y:Int}> 
	{
		var pts = [];
		var swapXY = FlxMath.absInt( y1 - y0 ) > FlxMath.absInt( x1 - x0 );
		var tmp : Int;
		if ( swapXY ) {
			// swap x and y
			tmp = x0; x0 = y0; y0 = tmp; // swap x0 and y0
			tmp = x1; x1 = y1; y1 = tmp; // swap x1 and y1
		}
     
		if ( x0 > x1 ) {
			// make sure x0 < x1
			tmp = x0; x0 = x1; x1 = tmp; // swap x0 and x1
			tmp = y0; y0 = y1; y1 = tmp; // swap y0 and y1
		}
     
		var deltax = x1 - x0;
		var deltay = fastFloor( FlxMath.absInt( y1 - y0 ) );
		var error = fastFloor( deltax / 2 );
		var y = y0;
		var ystep = if ( y0 < y1 ) 1 else -1;
		if ( swapXY )
		{
			// Y / X
			for ( x in x0 ... x1 + 1 ) 
			{
				pts.push({x:y, y:x});
				error -= deltay;
				if ( error < 0 ) {
					y = y + ystep;
					error = error + deltax;
				}
			}
		} else {
			// X / Y
			for ( x in x0 ... x1 + 1 ) 
			{
				pts.push({x:x, y:y});
				error -= deltay;
				if ( error < 0 ) {
					y = y + ystep;
					error = error + deltax;
				}
			}
		}
		return pts;
	}
	
	public static function convertMapArrayToString(map:Array<Array<Int>>):String
	{
		var mapString:String = "";
		
		for (y in 0...map.length)
		{
			for (x in 0...map[y].length)
			{
				mapString += Std.string(map[y][x]) + ",";
			}
			mapString += "\n";
		}
		return mapString;
	}

	public static function newMap(cols: Int, rows: Int):Array<Array<Int>>
	{
		var map:Array<Array<Int>> = new Array<Array<Int>>();
		for (y in 0...rows)
		{
			map.push(new Array<Int>());
			for (x in 0...cols)
			{
				map[y].push(1);
			}
		}
		return map;
	}
	
	public static function copyMap(map: Array<Array<Int>>):Array<Array<Int>>
	{
		var tmp: Array<Array<Int>>;
		
		tmp = new Array<Array<Int>>();
		for (y in 0...map.length)
		{
			tmp.push(new Array<Int>());
			for (x in 0...map[y].length)
			{
				tmp[y].push(map[y][x]);
			}
		}
		return tmp;
	}
	

	public static function generateNoise(cols: Int, rows: Int, noise:Int = 40):Array<Array<Int>>
	{
		var map:Array<Array<Int>> = newMap(cols, rows);
		for (y in 1...rows-1)
		{
			for (x in 1...cols-1)
			{
				map[y][x] = (FlxG.random.bool(noise) ? 1 : 0);
			}
		}
		return map;
	}
	
	/*o=-
	 * Generate a map array for a cavern-style level
	 * 
	 * cols:		how many tiles wide is the map
	 * rows: 		how many tiles high is the map
	 * firstPass:	how many adjacent tiles to determine wall/floor (1 distance)
	 * secondPass:	how many adjacent tiles to determine wall/floor (2 distance)
	 * 
	 * returns: 	a map array of tile types (0 = floor, 1 = wall)
	 */
	public static function generateCavern(cols: Int, rows: Int):FlxTilemap
	{
		var RLmap:FlxTilemap = new FlxTilemap();
		var map:Array<Array<Int>> = generateNoise(cols, rows, 45);
		var mapTmp:Array<Array<Int>> = newMap(cols, rows);
		var chambers:Array<Array<FlxPoint>> = new Array<Array<FlxPoint>>();
		var mainChamber:Int = 0;

		for (y in 0...3)
		{
			doCellularAutomata(map, mapTmp, 5, 2);
			map = mapTmp;
 		}
		for (y in 0...2)
		{
			doCellularAutomata(map, mapTmp, 5, -1);
			map = mapTmp;
		}
		
		floodFillCaverns(map, cols, rows, chambers);
		
		var lastChamberSize:Int = 0;
		for (i in 0...chambers.length)
		{
			if (chambers[i].length > lastChamberSize)
			{
				lastChamberSize = chambers[i].length;
				mainChamber = i;
			}
		}
		
		RLmap.loadMapFrom2DArray(map,"assets/images/tiles.png", RLDefs.TILE_WIDTH, RLDefs.TILE_HEIGHT);
		RLmap.setTileProperties(0, FlxObject.NONE);
		RLmap.setTileProperties(1, FlxObject.NONE, null, null, 1, 20);

		var pathf: FlxPathfinding;
		for (i in 0...chambers.length)
		{
			if (i != mainChamber)
			{	
				var origin:FlxPoint = chambers[i][0];
				var target:FlxPoint = chambers[mainChamber][0];
				pathf = new FlxPathfinding(RLmap);
				pathf.allowDiagonal = false;
				var pathPoints: Array<FlxPoint> = pathf.findPath(origin, target, true);
				if (pathPoints != null)
				{
					var lastpp:FlxPoint = origin;
					for (pp in pathPoints)
					{
						var flipPoints = getLine(cast(lastpp.x,Int), cast(lastpp.y,Int), cast(pp.x,Int), cast(pp.y,Int));
						for (px in flipPoints)
						{
							if (RLmap.getTile(px.x, px.y) == 1)
							{
								RLmap.setTile(px.x, px.y, 0, true);
							}
						}
						lastpp = pp;
					}
				}
			}
		}
		RLmap.setTileProperties(1, FlxObject.ANY);
		return RLmap;
	}
	
	private static function doCellularAutomata(inMap:Array<Array<Int>>, outMap:Array<Array<Int>>, firstPass:Int, secondPass:Int)
	{
		var firstPassCount:Int = 0;
		var secondPassCount:Int = 0;
		
		for (y in 1...inMap.length-1)
		{
			for (x in 1...inMap[y].length-1)
			{
				firstPassCount = countAdjacent(inMap, x, y, 1);
				if (secondPass >= 0)
				{
					secondPassCount = countAdjacent(inMap, x, y, 2);
				}
				if (firstPassCount >= firstPass || secondPassCount <= secondPass)
				{
					outMap[y][x] = 1;
				} else {
					outMap[y][x] = 0;
				}
			}
 		}
	}
	
	private static function countAdjacent(map:Array<Array<Int>>, PosX: Int, PosY:Int, distance: Int = 1):Int
	{
		var count: Int = 0;
		var rows: Int = map.length;
		var cols: Int = map[0].length;
		var y: Int;
		var x: Int;
		
		for (y in (-distance)...(distance+1))
		{
			for (x in (-distance)...(distance+1))
			{
				if ((PosX + x < 0) || (PosX + x > cols - 1) || (PosY + y < 0) || (PosY + y > rows - 1))
				{
					continue;
				}
				if (distance == 2 && FlxMath.absInt(y - PosY) == 2 && FlxMath.absInt(x - PosX) == 2)
				{
					continue;
				}
				if (map[PosY + y][PosX + x] != 0)
				{
					count++;
				}
			}
		}
		return count;
	}
	
	/*o=-
	 * Input map data, get back array of unique chambers
	 */
	public static function floodFillCaverns(map:Array<Array<Int>>, cols:Int, rows:Int, chambers:Array<Array<FlxPoint>>)
	{
		var tmpMap: Array<Array<Int>> = new Array<Array<Int>>();
		
		tmpMap = copyMap(map);
		
		var fillCount = 100;
		for (y in 1...map.length-1)
		{
			for (x in 1...map[y].length-1)
			{
				if (tmpMap[y][x] == 0)
				{
					chambers.push(new Array<FlxPoint>());
					floodFillCave(tmpMap, y, x, fillCount, chambers);
					fillCount++;
				}
			}
		}
	}

	private static function floodFillCave(tmpMap:Array<Array<Int>>, PosY:Int, PosX:Int, fillCount:Int, chambers:Array<Array<FlxPoint>>)
	{
		if (tmpMap[PosY][PosX] != 0)
		{
			return;
		}
		
		tmpMap[PosY][PosX] = fillCount;
		chambers[chambers.length - 1].push(new FlxPoint(PosX, PosY));
		
		if (PosY > 1)
		{
			floodFillCave(tmpMap, PosY - 1, PosX, fillCount, chambers);
		}
		if (PosY < tmpMap.length - 2)
		{
			floodFillCave(tmpMap, PosY + 1, PosX, fillCount, chambers);
		}
		if (PosX > 1)
		{
			floodFillCave(tmpMap, PosY, PosX - 1, fillCount, chambers);
		}
		if (PosX < tmpMap[PosY].length - 2)
		{
			floodFillCave(tmpMap, PosY, PosX + 1, fillCount, chambers);
		}
	}
	
	public static function convertIntToTile(inInt: Int):RLTileTypes
	{	
		var ttype: RLTileTypes;
		ttype = EnumTools.createByIndex(RLTileTypes, inInt);
		return ttype;
	}
	
	public static function generateDungeon(cols:Int, rows:Int, lifeMin:Int, lifeMax:Int, turnMin:Int, turnMax:Int, tspMin:Int, tspMax:Int, rspMin:Int, rspMax:Int, neatMin:Int, neatMax:Int, chanceDoor:Int):FlxTilemap
	{
		var tunneler: RLTunneler;
		var tunnelerQueue: Array<RLTunneler>;
		var map: Array<Array<Int>>;
		var retMap: FlxTilemap;
		var qIterate: Int = 0;
		var xc: Int;
		var yc: Int;
		var xdir: Int;
		var ydir: Int;
		var newname = 2;
		var cntDug = 0;
		
		map = newMap(cols, rows);
		tunneler = new RLTunneler(lifeMin, lifeMax, turnMin, turnMax, tspMin, tspMax, rspMin, rspMax, neatMin, neatMax);
		
		tunneler.DirX = 0;
		tunneler.DirY = 1;
		tunneler.PosX = cast(cols / 2, Int);
		tunneler.PosY = cast(rows / 2, Int);
		
		tunnelerQueue = new Array<RLTunneler>();
		tunnelerQueue.insert(0, tunneler);
		var tQueueMaxSize: Int = 50;
		
		while (tunnelerQueue.length > 0 && qIterate < 2500)
		{
			qIterate++;
			tunneler = tunnelerQueue.pop();
			xc = tunneler.PosX;
			yc = tunneler.PosY;
			xdir = tunneler.DirX;
			ydir = tunneler.DirY;
			
			var validMove: Int = 1;
			var chkSQx: Int;
			var chkSQy: Int;
			var cntFloors: Int = 0;
			
			for (chkSQx in -1...2)
			{
				var mapCx: Int;
				var breakX: Bool = false;
				mapCx = xc + xdir + chkSQx;
				if (mapCx > 0 && mapCx < cols - 1)
				{
					for (chkSQy in -1...2)
					{
						var mapCy: Int;
						mapCy = yc + ydir + chkSQy;
						if (mapCy > 0 && mapCy < rows - 1)
						{
							var ttype: RLTileTypes;
							ttype = EnumTools.createByIndex(RLTileTypes, map[mapCy][mapCx]);
							switch (ttype) 
							{
								case TILE_FLOOR: 	cntFloors++;
								case TILE_ROOM: 	validMove = 0;
													breakX = true;
													break;
								default: //do nothing
							}
						}
					}
					if (breakX) break;
				}
			}
			var xCond, yCond: Int;
			xCond = xc + (xdir * 2);
			yCond = yc + (ydir * 2);
			if (xCond > 0  && xCond < RLDefs.MAP_WIDTH - 1 && yCond > 0 && yCond < RLDefs.MAP_HEIGHT - 1)
			{
				var ttype: RLTileTypes = convertIntToTile(map[yCond][xCond]);
				
				if (ttype == TILE_ROOM)
				{
					var chkz: Int;
					chkz = 1;
					for (zz in -1...2)
					{
						var TT: Int;
						TT = map[yc + ydir + (xdir * zz)][xc + xdir + (ydir * zz)];
						var ttypeb:RLTileTypes;
						ttypeb = convertIntToTile(TT);
						if (ttypeb == TILE_O_DOOR || ttypeb == TILE_C_DOOR)
						{
							chkz = 0;
						}
					}
					if (chkz == 1 && FlxG.random.bool(chanceDoor))
					{
						if (FlxG.random.bool(75))
						{
							map[yc + ydir][xc + xdir] = EnumValueTools.getIndex(TILE_C_DOOR);
						} else {
							map[yc + ydir][xc + xdir] = EnumValueTools.getIndex(TILE_O_DOOR);
						}
					}
					validMove = 0;
				}
			}
			if (cntFloors > tunneler.Neatness)
			{
				validMove = 0;
			}
			if (xc + xdir > 0 && xc + xdir < RLDefs.MAP_WIDTH -1 && yc + ydir > 0 && yc + ydir < RLDefs.MAP_HEIGHT - 1 && validMove == 1)
			{
				map[yc + ydir][xc + xdir] = EnumValueTools.getIndex(TILE_FLOOR);
				cntDug++;
				tunneler.PosX = xc + xdir;
				tunneler.PosY = yc + ydir;
				// Check if spawning a roomer, and try to spawn room
				if (FlxG.random.bool(tunneler.RoomerSpawnChance))
				{
					var dp: Bool;
					dp = FlxG.random.bool(50);
					var dirx = 0;
					var diry = 0;
					if (tunneler.DirX == 0)
					{
						if (dp) { dirx = -1; } else { dirx = 1; }
					} else {
						if (dp) { diry = -1; } else { diry = 1; }
					}
					var dx, dy: Int;
					dx = tunneler.PosX + dirx;
					dy = tunneler.PosY + diry;
					var validDoor:Int = 1;
					if (dx < 1 || dx > RLDefs.MAP_WIDTH - 1 || dy < 1 || dy > RLDefs.MAP_HEIGHT - 1)
					{
						validDoor = 0;
					}
					if (validDoor == 1 && convertIntToTile(map[dy][dx]) == TILE_WALL)
					{
						var roomer: RLRoomer;
						roomer = new RLRoomer(dx, dy, dirx, diry);
						var chk_bnd: Int;
						if (dirx == 0) { chk_bnd = roomer.DimensionX; } else { chk_bnd = roomer.DimensionY; }
						var validRoom: Int = 1;
						for (i_bnd in FlxG.random.int(0, chk_bnd - 1)...chk_bnd)
						{
							var absx: Int = Std.int(Math.abs(dirx) * i_bnd);
							var absy: Int = Std.int(Math.abs(diry) * i_bnd);
							roomer.getRoomCorners(absx, absy);
							validRoom = 1;
							if (roomer.UpperLeft.x <= 0 || roomer.UpperLeft.x >= RLDefs.MAP_WIDTH - 1 || roomer.UpperLeft.y <= 0 || roomer.UpperLeft.y >= RLDefs.MAP_HEIGHT -1) validRoom = 0;
							if (roomer.UpperRight.x <= 0 || roomer.UpperRight.x >= RLDefs.MAP_WIDTH - 1 || roomer.UpperRight.y <= 0 || roomer.UpperRight.y >= RLDefs.MAP_HEIGHT -1) validRoom = 0;
							if (roomer.LowerLeft.x <= 0 || roomer.LowerLeft.x >= RLDefs.MAP_WIDTH - 1 || roomer.LowerLeft.y <= 0 || roomer.LowerLeft.y >= RLDefs.MAP_HEIGHT -1) validRoom = 0;
							if (roomer.LowerRight.x <= 0 || roomer.LowerRight.x >= RLDefs.MAP_WIDTH - 1 || roomer.LowerRight.y <= 0 || roomer.LowerRight.y >= RLDefs.MAP_HEIGHT -1) validRoom = 0;
							var icol_break: Bool = false;
							if (validRoom == 1)
							{
								for (ixcollide in Std.int(roomer.UpperLeft.x - 1)...Std.int(roomer.LowerRight.x + 2))
								{
									for (iycollide in Std.int(roomer.UpperLeft.y - 1)...Std.int(roomer.LowerRight.y + 2))
									{
										if (convertIntToTile(map[iycollide][ixcollide]) != TILE_WALL)
										{	
											validRoom = 0;
											icol_break = true;
											break;
										}
									}
									if (icol_break) { break; }
								}
							}
							if (validRoom == 1)
							{
								for (ixdig in Std.int(roomer.UpperLeft.x)...Std.int(roomer.LowerRight.x + 1))
								{
									for (iydig in Std.int(roomer.UpperLeft.y)...Std.int(roomer.LowerRight.y + 1))
									{
										map[iydig][ixdig] = EnumValueTools.getIndex(TILE_ROOM);
										cntDug++;
									}
								}
								if (FlxG.random.bool(75))
								{
									map[roomer.DoorPosY][roomer.DoorPosX] = EnumValueTools.getIndex(TILE_C_DOOR);
								} else {
									map[roomer.DoorPosY][roomer.DoorPosX] = EnumValueTools.getIndex(TILE_O_DOOR);
								}
								break;
							}
						}
					}
				}
				if (FlxG.random.bool(tunneler.TurnChance))
				{
					if (tunneler.DirX == 0)
					{
						tunneler.DirY = 0;
						if (FlxG.random.bool(50)) { tunneler.DirX = -1; } else { tunneler.DirY = 1; }
					} else {
						tunneler.DirX = 0;
						if (FlxG.random.bool(50)) {tunneler.DirY = -1; } else { tunneler.DirY = 1; }
					}
				}
				if (FlxG.random.bool(tunneler.TunnelerSpawnChance))
				{
					var tunnelerTemp: RLTunneler = new RLTunneler(lifeMin, lifeMax, turnMin, turnMax, tspMin, tspMax, rspMin, rspMax, neatMin, neatMax);
					newname++;
					tunnelerTemp.PosX = tunneler.PosX;
					tunnelerTemp.PosY = tunneler.PosY;
					tunnelerTemp.DirX = 0;
					tunnelerTemp.DirY = 0;
					if (tunneler.DirX == 0)
					{
						if (FlxG.random.bool(50)) { tunnelerTemp.DirX = -1; } else { tunnelerTemp.DirX = 1; }
					} else {
						if (FlxG.random.bool(50)) { tunnelerTemp.DirY = -1; } else { tunnelerTemp.DirY = 1; }
					}
					if (tunnelerQueue.length + 1 <= tQueueMaxSize)
					{
						tunnelerQueue.insert(0, tunnelerTemp);
					}
				}
				if (tunneler.Age < tunneler.Lifetime)
				{
					tunneler.Age++;
					tunnelerQueue.insert(0, tunneler);
				}
			}
			//trace("Queue: " + tunnelerQueue.length + " - Iterate: " + qIterate + " - Dug: " + (cntDug / (cols * rows)) * 100 + "%");
			if ((tunnelerQueue.length == 0 || qIterate >= 2500) && cntDug < Std.int(RLDefs.MAP_WIDTH * RLDefs.MAP_HEIGHT * 0.35))
			{	
				trace("Map sucks, re-rolling");
				map = new Array<Array<Int>>();
				map = newMap(RLDefs.MAP_WIDTH, RLDefs.MAP_HEIGHT);
				cntDug = 0;
				qIterate = 0;
				tunneler = new RLTunneler(lifeMin,lifeMax,turnMin,turnMax,tspMin,tspMax,rspMin,rspMax,neatMin,neatMax);
				tunneler.PosX = cast(cols / 2, Int);
				tunneler.PosY = cast(rows / 2,Int);
				newname = 2;
				tunnelerQueue = new Array<RLTunneler>();
				tunnelerQueue.insert(0, tunneler);
			}
		}
		retMap = new FlxTilemap();
		retMap.loadMapFrom2DArray(map, "assets/images/tiles.png", RLDefs.TILE_WIDTH, RLDefs.TILE_HEIGHT, OFF, 0,0);
		return retMap;
	}
	
}