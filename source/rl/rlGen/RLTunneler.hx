package rl.rlGen;

import flixel.FlxG;

/**
 * ...
 * @author 
 */
class RLTunneler
{
	public var PosX:Int = 0;
	public var PosY:Int = 0;
	public var DirX:Int = 0;
	public var DirY:Int = 0;
	public var Age:Int = 0;
	public var Lifetime:Int = 0;
	public var TurnChance:Int = 50;
	public var TunnelerSpawnChance:Int = 50;
	public var RoomerSpawnChance:Int = 50;
	public var Neatness:Int = 0;
	
	public function new(lifeMin:Int, lifeMax:Int, turnMin:Int, turnMax:Int, tSpawnMin:Int, tSpawnMax:Int, rSpawnMin:Int, rSpawnMax:Int, neatnessMin:Int, neatnessMax:Int)
	{
		DirX = 0;
		DirY = 0;
		if (FlxG.random.bool(50))
		{
			if (FlxG.random.bool(50))
			{
				DirX = -1;
			} else {
				DirX = 1;
			}
		} else {
			if (FlxG.random.bool(50))
			{
				DirY = -1;
			} else {
				DirY = 1;
			}
		}
		Lifetime = (FlxG.random.int(lifeMin, lifeMax));
		Age = 0;
		TurnChance = (FlxG.random.int(turnMin, turnMax));
		TunnelerSpawnChance = (FlxG.random.int(tSpawnMin, tSpawnMax));
		RoomerSpawnChance = (FlxG.random.int(rSpawnMin, rSpawnMax));
		Neatness = (FlxG.random.int(neatnessMin, neatnessMax));
	}
}